package com.github.mikephil.charting.renderer

import android.graphics.Canvas
import com.github.mikephil.charting.animation.ChartAnimator
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.utils.ViewPortHandler

class CustomBarRenderer constructor(
        chart: BarChart,
        animator: ChartAnimator,
        vpHandler: ViewPortHandler,
        cornerDimens: Float
) : BarChartRenderer(chart, animator, vpHandler) {
    init {

    }

    override fun drawDataSet(c: Canvas, dataSet: IBarDataSet, index: Int) {

    }
}